branch=${1:-master}

echo "Updating from branch $branch"
git fetch --all
git reset --hard origin/$branch
bash fix_rights.sh