sudo apt-get update

echo ""
echo "======= Installing: linux-headers, video-driver, gcc-6, pip, ffmpeg, feh ======="
sudo apt-get -y -q install linux-headers-$(uname -r) nvidia-384 gcc-6 g++-6 python-pip ffmpeg feh

echo ""
echo "======= Installing: python packages ======="
pip install tensorflow-gpu==1.10.1 opencv-python==3.4.3.18 requests numpy scipy

echo ""
echo "======= Installing : symlinks (ignore errors) ======="
sudo rm /usr/bin/gcc
sudo rm /usr/bin/g++
sudo rm /usr/local/cuda

sudo rm /usr/bin/gcc
sudo rm /usr/bin/g++
sudo ln -s /usr/bin/gcc-6 /usr/bin/gcc
sudo ln -s /usr/bin/g++-6 /usr/bin/g++
sudo rm /usr/local/cuda
sudo ln -s /usr/local/cuda-9.0 /usr/local/cuda
echo ""

echo "======= Installing : CUDA ======="
sudo bash ./_install/cuda_9.0.176_384.81_linux.run --silent --toolkit

echo ""
echo "======= Installing: cudnn ======="
CUDNN_PKG="libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb"
wget https://github.com/ashokpant/cudnn_archive/raw/master/v7.0/${CUDNN_PKG}
sudo dpkg -i ${CUDNN_PKG}
sudo apt-get update
rm -rf ${CUDNN_PKG}

echo ""
echo "======= Installing : exporting paths ======="
echo '# cuda' >> ~/.bashrc
echo 'export PATH=/usr/local/cuda/bin:$PATH' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH' >> ~/.bashrc
echo 'export CUDA_HOME=${CUDA_HOME}:/usr/local/cuda:/usr/local/cuda-9.0' >> ~/.bashrc

sudo echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf

echo ""
echo "======= Installing : evaluating paths ======="
source ~/.bashrc
sudo ldconfig

echo ""
echo "======= Installing : DONE ======="
echo ""
echo ""
echo ""