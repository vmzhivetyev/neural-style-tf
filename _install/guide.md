# Ubuntu

1) Качаешь wubi [.exe](https://github.com/hakuna-m/wubiuefi/releases/download/18041r333/wubi18041r333.exe) [GitHub](https://github.com/hakuna-m/wubiuefi/releases/tag/18041r333)

2) Ставишь с помощью нее **Ubuntu 18.04.1** (это изи)


##### P.S. [why wubi is bad](https://ubuntuforums.org/showthread.php?t=481219)


## После успешной загрузки в Ubuntu
~~1) Где-то там настраиваешь пользователя~~

1) Открываешь `Файлы`

2) `Домашняя папка`

3) Правый клик в пустоту и `Открыть в терминале`

4) ~~Клонишь репу~~ Вставляешь строчки

```
sudo apt install git
git clone https://bitbucket.org/vmzhivetyev/neural-style-tf.git
cd neural-style-tf

sudo bash fix_rights.sh

source install.sh
```

5) Забываем про комп минут на 30 (мб больше - хз)

6) Как только появится надпись `Installation completed.`

скидываем файлик `install.log` из появившейся папки `neural-style-tf` (в домашней папке) мне

## Использование

1) Закидываем видосики в `neural-style-tf/video_input`

2) Закидываем картинки стилей в `neural-style-tf/styles` (несколько там уже есть)

2) В той самой папке `neural-style-tf` нажимаем `Открыть в терминале`

2) Пишем

```
./stylize_video.sh "video_input/[имя файла видоса с расширением]" "styles/[имя файла картинки стиля с расширением]" [вес контента] [вес стиля]

Пример:
./stylize_video.sh "video_input/video.mp4" "styles/the_scream.jpg" 1 10
```

Соответственно, чем больше отношения веса стиля к весу контента, тем более неузнаваемым будет изначальный видос.

Когда увидим надпись `Saved to file: [путь к видосу, который сохранился]`, 
можно смотреть, что же получилось 🤔
