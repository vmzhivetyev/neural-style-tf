
echo "Installation log:
" > install.log

(source deps_install.sh >> install.log; \
source deps_check.sh >> install.log) &

install_pid=$!

echo "Download model if needed:"
source check_model.sh

echo "Waiting for installation to complete..."
wait $install_pid
echo "Installation completed."

echo ""
echo "======== ALL DONE ========"
echo ""
echo ""
echo "RUNNING EXAMPLE: "
echo "./_EXAMPLE.sh"
echo ""
echo ""
echo ""
echo ""
read -p "Reboot now? [y/n] > " doreb

if [ "$doreb" == "y" ]; then
  reboot
fi