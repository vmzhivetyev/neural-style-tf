image_pattern=*.png
folder=${1:-./}
extension="${image_pattern##*.}" # png

cd $folder

echo "Converting $folder/$image_pattern files to video..."

files=($(ls ${image_pattern}))

count=${#files[*]}

a=1
for i in ${files[*]}; do
  new=$(printf "image%04d.${extension}" "$a")
  echo "$i -> $new"
  # cp -f -i -- "$i" "$new"
  let a=a+1
  annotation=$i
  convert $i -gravity south -pointsize 24 -stroke '#000C' -strokewidth 2 -annotate 0 "$annotation" \
  -stroke none -fill white -annotate 0 "$annotation" $new;
done

folder=$(basename $folder)
ffmpeg -y -f image2 -i image%04d.${extension} ../${folder:-video}.mp4

a=$count
while [ $a -gt 0 ]; do
  new=$(printf "image%04d.${extension}" "$a")
  # echo "rm -f $new"
  rm -f "$new"
  let a=a-1
done