set -e
# Get a carriage return into `cr`
cr=`echo $'\n.'`
cr=${cr%.}

# Find out whether ffmpeg or avconv is installed on the system
FFMPEG=ffmpeg
command -v $FFMPEG >/dev/null 2>&1 || {
  FFMPEG=avconv
  command -v $FFMPEG >/dev/null 2>&1 || {
    echo >&2 "This script requires either ffmpeg or avconv installed.  Aborting."; exit 1;
  }
}

if [ "$#" -le 1 ]; then
   echo "Usage: bash stylize_video.sh <path_to_video> <path_to_style_image> [content_weight] [style_weight]"
   exit 1
fi

if [ ! -f "$1" ]; then
  echo "Error: no such file: \"$1\""
  exit 1
fi

if [ ! -f "$2" ]; then
  echo "Error: no such file: \"$2\""
  exit 1
fi

# Parse arguments
content_video="$1"
content_dir=$(dirname "$content_video")
content_filename=$(basename "$content_video")
extension="${content_filename##*.}"
content_filename="${content_filename%.*}"
content_filename=${content_filename//[%]/x}

style_image="$2"
style_dir=$(dirname "$style_image")
style_filename=$(basename "$style_image")

if [ ! -d "./video_input" ]; then
  mkdir -p ./video_input
fi
temp_dir="./video_input/${content_filename}"

# Create output folder
mkdir -p "$temp_dir"



# Clean previous out frames
echo "Cleanup: rm video_output/*.ppm"
(rm video_output/*.ppm) &

echo "Splitting video"

# Save frames of the video as individual image files
$FFMPEG -v quiet -i "$1" "${temp_dir}/frame_%04d.ppm"
eval $(ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=width,height "$1")
width="${streams_stream_0_width}"
height="${streams_stream_0_height}"
if [ "$width" -gt "$height" ]; then
  max_size="$width"
else
  max_size="$height"
fi
num_frames=$(find "$temp_dir" -iname "*.ppm" | wc -l)

echo "Splitted $num_frames frames"


parallelize_opt_flow=true

compute_flow () {
  # $1 content filename e.g. "moon"
  # $2 start frame
  # $3 end frame
  echo "Computing optical flow for frames $2 to $3 [CPU]"
  cd ./video_input
  ./make-opt-flow.sh $1/frame_%04d.ppm $1 $2 1 $3
  cd ..
}

compute_flow_parallel() {
  if $parallelize_opt_flow; then
    compute_flow $content_filename $1 $2
  fi
}

if $parallelize_opt_flow; then
    echo "Parallelize optical flow: Enabled"
else
    echo "Parallelize optical flow: Disabled"

    echo "Computing full optical flow [CPU]. This will take a while..."
  compute_flow $content_filename 1 $num_frames
fi



# fork for multigpu
dev_list=$(python gpu-list.py)
devices=($dev_list)
n_devices=${#devices[*]}
start_frame=1

echo "Rendering stylized video frames [CPU & GPU]. This will take a while..."



# start_frame=273
# num_frames=270
frame_iterations=${5:-100}
first_frame_iterations=$(($frame_iterations*2))



echo "Using devices ${devices[*]} to render $num_frames frames"

# run processes and store pids in array
for index in ${!devices[*]}; do
  end_frame=$(((1+index)*num_frames/n_devices))
  device=${devices[index]}
  echo $device will render frames $start_frame to $end_frame; 

      # this was inside of fork to copy a content image like it was previos styled frame which is really missing
      #
      # prev_frame=$(($start_frame-1)); \
      # prev_frame=`printf "frame_%04d.ppm" $prev_frame`; \
      # echo $prev_frame; cp "video_input/$content_filename/$prev_frame" "video_output/$prev_frame" -v -n; 

      # firstly start computing optical flow and then start style tranfer with delay
    (eval "(compute_flow_parallel $start_frame $end_frame) &"; sleep 10; \
\
python neural_style.py --video \
--video_input_dir "${temp_dir}" \
--style_imgs_dir "${style_dir}" \
--style_imgs "${style_filename}" \
--start_frame "${start_frame}" \
--device "${device}" \
--end_frame "${end_frame}" \
--max_size "${max_size}" \
--optimizer "lbfgs" \
--first_frame_iterations "$first_frame_iterations" \
--frame_iterations "$frame_iterations" \
--content_weight "${3:-5}" \
--style_weight "${4:-1000}" \
;) &

    #
    # DEFAULT:
      # --content_weight "5" \
      # --style_weight "1000" \
      # --original_colors \
    #

    pids[${index}]=$!

    sleep 3
    
    # echo "REMOVE THIS: WAITING TO COMPLETE"
    echo "${device} : $!"
    #wait $!
    echo ""
    echo ""

    start_frame=$(($end_frame + 1))
done

# wait for all pids
for pid in ${pids[*]}; do
    echo ""
    echo ""
    echo final wait $pid
    echo ""
    echo ""
    wait $pid
    echo $pid ended
    echo ""
    echo ""
    echo ""
done




# Create video from output images.
echo "Converting image sequence to video.  This should be quick..."
video_file="./video_output/${style_filename%.*}/${content_filename}-c:$3-s:$4.$extension"
if [ ! -d "./video_output/${style_filename%.*}" ]; then
  mkdir -p ./video_output/${style_filename%.*}
fi

# storing frames
if [ ! -d "${video_file}-frames" ]; then
  mkdir -p "${video_file}-frames"
  cp video_output/*.ppm "${video_file}-frames"
fi

$FFMPEG -v quiet -i "./video_output/frame_%04d.ppm" "${video_file}"

echo "Saved to file: $video_file"

# Clean up garbage
# if [ -d "${temp_dir}" ]; then
#   rm -rf "${temp_dir}"
# fi
