set -e
# Get a carriage return into `cr`
cr=`echo $'\n.'`
cr=${cr%.}

if [ "$#" -le 1 ]; then
   echo "Usage: bash stylize_image.sh <path_to_content_image> <path_to_style_image>"
   exit 1
fi

if [ ! -f "$1" ]; then
	echo "Error: no such file: \"$1\""
	exit 1
fi

if [ ! -f "$2" ]; then
	echo "Error: no such file: \"$2\""
	exit 1
fi

device='/gpu:0'

# Parse arguments
content_image="$1"
content_dir=$(dirname "$content_image")
content_filename=$(basename "$content_image")

style_image="$2"
style_dir=$(dirname "$style_image" )
style_filename=$(basename "$style_image")

content_weight=${3:-5}
style_weight=${4:-1000}
max_iterations=${5:-200}
max_size=${6:-512}
max_iterations_steps=${7:-1}
result_filename="${content_filename%.*}+${style_filename%.*}_c=${content_weight}_s=${style_weight}_i=${max_iterations}_s=${max_size}"

echo "Rendering stylized image. This may take a while..."
python neural_style.py \
--content_img "${content_filename}" \
--content_img_dir "${content_dir}" \
--style_imgs "${style_filename}" \
--style_imgs_dir "${style_dir}" \
--device "${device}" \
--max_iterations "$max_iterations" \
--content_weight "$content_weight" \
--style_weight "$style_weight" \
--img_name "$result_filename" \
--max_size "$max_size" \
--max_iterations_steps "$max_iterations_steps";

if [ -d image_output/$result_filename ]; then
  (bash video_from_images.sh image_output/$result_filename) &
fi

# mv ./image_output/$result_filename/${result_filename}.png image_output/
# rm -rf ./image_output/$result_filename

# (feh ./image_output/${result_filename}.png) &