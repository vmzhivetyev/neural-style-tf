

vgg19filename="imagenet-vgg-verydeep-19.mat"
if md5sum --status -c "${vgg19filename}.md5"; then
  echo "$vgg19filename MD5: OK"
else
  echo "$vgg19filename MD5: BAD"
  if [ -f $vgg19filename ]; then
    rm -rf $vgg19filename
  fi
  wget http://www.vlfeat.org/matconvnet/models/$vgg19filename
fi