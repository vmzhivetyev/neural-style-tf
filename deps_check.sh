echo
echo
echo
echo
echo ====== Checking installation ======
echo ====== gcc =======
echo
gcc --version

echo
echo ====== cuda =======
echo
which nvcc
echo
nvcc -V
echo
nvidia-smi

echo
echo ====== pip =======
echo
pip --version

echo
echo ====== tensorflow =======
echo
python -c "import tensorflow as tf; print('tensorflow version', tf.__version__)"

echo
echo ====== ffmpeg =======
ffmpeg -version

echo ====== --------------------- ======
echo